from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='dragonfly_wrapper',
      version='0.1',
      description='Module to integrate Dragonfly (github.com/duaneloh/Dragonfly) into SPI workflow',
      long_description=readme(),
      long_description_content_type="text/markdown",
      url='https://gitlab.com/spi_xfel/dragonfly_wrapper',
      author='Sergey Bobkov',
      author_email='s.bobkov@grid.kiae.ru',
      license='MIT',
      python_requires='>=3.6',
      install_requires=['numpy',
                        'h5py'],
      packages=['dragonfly_wrapper'],
      scripts=['scripts/dragonfly_add.py',
               'scripts/dragonfly_create.py',
               'scripts/dragonfly_extract.py'],
      zip_safe=False)
