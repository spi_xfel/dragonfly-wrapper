# Installation

Processing scripts require **Python 3.6** or higher.

You can install package by:
```
cd dragonfly_wrapper
pip install .
```

# File format

Input data should be in [CXI format](https://www.cxidb.org/cxi.html).
The following internal structure is expected:
```
file.cxi 
   ├─ cxi_version
   └─ entry_1
       ├─ image_1
       │   ├─ data (N, y, x)
       │   ├─ mask (y, x)
       │   └─ image_center (3) [x, y, z]
       │
       ├─ image_2
       │   ├─ data
       │   ├─ mask
       │   └─ image_center
...
       └─ image_M
           ├─ data
           ├─ mask
           └─ image_center

```
Each **image_k** group should have **data** dataset with images, **mask** dataset indicate bad pixels and **image_center** dataset contain coordinates of image center in pixels.
Other groups and datasets are optional.

Scripts follow presented internal structure. Images are combined in the same **image_k** group if they have the same **mask** and **image_center** values.


# EMC with Dragonfly 

## Create new Dragonfly reconstruction directory

Init new Dragonfly directory with configuration

```
dragonfly_create.py [-h] --num-div N [--beta START MULTIPLIER STEP] dir
```

Options:
* `dir` - name of reconstruction directory
* `--num-div N` - refinement of the quasi-uniform rotation sampling. Recommended values from 6 to 10
* `--beta START MULTIPLIER STEP` - schedule for deterministic annealing parameter. Dragonfly will begin with START value which will be multiplied by MULTIPLIER every STEP iterations

## Add CXI data to Dragonfly reconstruction

Convert CXI data into Dragonfly format and add to reconstruction config.

```
dragonfly_add.py [-h] [-b BINNING] [-r MIN_RADIUS] [-R MAX_RADIUS]
                 --wavelength WAVELENGTH --distance DISTANCE --pixel
                 PIXEL [--polarisation {x,y,none}] [--q_mul Q_MUL]
                 [--hdf]
                 cxi_file dir
```

Options:
* `cxi_file` - input CXI file
* `dir` - existing reconstruction directory
* `-b BINNING` - Bin the data
* `-r MIN_RADIUS` - Inner radius of image area to convert.
    Pixels within min_radius will not be used for orientation determination
* `--rsoft MIN_SOFT_RADIUS` - Minimal radius for orientationally relevant pixels
* `-R MAX_RADIUS` - Outer radius of image area. Pixels outside max_radius will be ignored
* `--wavelength WAVELENGTH` - Radiation wavelength (Angstrom)
* `--distance DISTANCE` - Detector distance (meters)
* `--pixel PIXEL` - Pixel size (meters)
* `--polarisation {x,y,none}` - Detector polarisation
* `--q_mul Q_MUL` - Global multiplier for q-space, disables autoscaling
* `--hdf` - Save output in new HDF5 Dragonfly format

## Run EMC

You need to run Dragonfly in created folder using your Dragonfly installation.

```
emc [-c CONFIG] [-t THREADS] [-r] ITER
```
Options:
* `-c CONFIG` - path to config file. Default: config.ini in working folder
* `-t THREADS` - number of computing threads
* `-r` - resume existing reconstruction
* `ITER` - number of iterations to perform

## Extract Dragonfly result

Convert Dragonfly result into CXI data.

```
dragonfly_extract.py [-h] [-i ITER] dir output
```

Options:
* `dir` - existing reconstruction directory
* `output` - name of output CXI file
* `-i ITER` - Iteration number. Default: select best iteration
