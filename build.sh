# /bin/sh

python setup.py sdist bdist_wheel

docker build -t dragonfly_wrapper .
docker build -t dragonfly_utils -f docker/dragonfly_utils .
docker build -t dragonfly -f docker/dragonfly .
