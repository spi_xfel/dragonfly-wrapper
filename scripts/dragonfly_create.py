#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create Dragonfly reconstruction directory
Author: Sergey Bobkov
"""

import os
import argparse
import configparser


def main():
    parser = argparse.ArgumentParser(description='Create Dragonfly reconstruction directory')
    parser.add_argument('dir', help="Reconstruction directory name")
    parser.add_argument('--num-div', dest='num_div', metavar='N', required=True, type=int,\
                        help="Number of divisions for quasi-uniform rotation sampling")
    parser.add_argument('--beta', dest='beta', type=float, nargs=3, \
                        metavar=('START', 'MULTIPLIER', 'STEP'), help="Beta schedule")
    parser_args = parser.parse_args()

    out_dir = parser_args.dir
    num_div = parser_args.num_div
    beta = parser_args.beta

    if os.path.exists(out_dir):
        parser.error("Directory {} already exist".format(out_dir))

    print('Create new reconstruction with:')
    print('Directory: {}'.format(out_dir))
    print('num_div: {}'.format(num_div))
    if beta:
        beta_start, beta_mul, beta_step = beta
        beta_step = int(beta_step)
        print('Beta schedule: {}:{}:{}'.format(beta_start, beta_mul, beta_step))

    os.makedirs(out_dir)

    config = configparser.ConfigParser()
    config['emc'] = {'in_photons_list': 'emc_files.dat',
                     'in_detector_list': 'det_files.dat',
                     'num_div': str(num_div),
                     'output_folder': 'data/',
                     'log_file': 'EMC.log',
                     'need_scaling': '1'}

    if beta:
        config['emc']['beta'] = str(beta_start)
        config['emc']['beta_schedule'] = '{} {}'.format(beta_mul, beta_step)

    config_fname = os.path.join(out_dir, 'config.ini')
    with open(config_fname, 'w') as cfile:
        config.write(cfile)


if __name__ == '__main__':
    main()
