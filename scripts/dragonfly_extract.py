#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Extract EMC result into CXI format
Author: Sergey Bobkov
"""

import os
import sys
import argparse
import configparser

from dragonfly_wrapper import extract


def main():
    parser = argparse.ArgumentParser(description='Extract EMC result into CXI format')
    parser.add_argument('dir', help="Reconstruction directory")
    parser.add_argument('output', help="Output CXI file")
    parser.add_argument('-i', '--iter', type=int,
                        help="Iteration number [default: select best iteration]")
    parser_args = parser.parse_args()

    recon_dir = parser_args.dir
    output_cxi = parser_args.output
    iteration = parser_args.iter

    if not os.path.exists(recon_dir):
        parser.error("Directory {} doesn't exist".format(recon_dir))

    config = configparser.ConfigParser()
    config_fname = os.path.join(recon_dir, 'config.ini')
    config.read(config_fname)

    log_file = os.path.join(recon_dir, config['emc']['log_file'])
    log_data = extract.parse_emc_log(log_file)

    if not log_data['Iter']:
        sys.stderr.write('No iterations was completed.')
        sys.exit(-1)

    if iteration is None:
        iteration = extract.get_best_iteration(log_data['beta'],
                                               log_data['rms_change'],
                                               log_data['log-likelihood'])
        iteration = int(log_data['Iter'][iteration])

    print('Saving iteration', iteration)

    emc_output_h5 = os.path.join(recon_dir,
                                 config['emc']['output_folder'],
                                 'output_{:03d}.h5'.format(iteration))

    emc_output_bin = os.path.join(recon_dir,
                                  config['emc']['output_folder'],
                                  'output/intens_{:03d}.bin'.format(iteration))

    if os.path.isfile(emc_output_h5):
        intens = extract.read_h5(emc_output_h5)
    elif os.path.isfile(emc_output_bin):
        intens = extract.read_bin(emc_output_bin)
    else:
        raise ValueError('Cannot find output data for both h5 and binary formats')

    extract.save_cxi(intens, output_cxi)


if __name__ == '__main__':
    main()
