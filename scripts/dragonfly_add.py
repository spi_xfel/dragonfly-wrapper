#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Add CXI data to Dragonfly reconstruction directory
Author: Sergey Bobkov
"""

import os
import argparse
import configparser

from dragonfly_wrapper.convert import convert_data


def main():
    parser = argparse.ArgumentParser(description='Add CXI data to Dragonfly reconstruction directory')
    parser.add_argument('cxi_file', help="Input CXI file")
    parser.add_argument('dir', help="Reconstruction directory")
    parser.add_argument('-b', dest='binning', type=int, help="Bin the data")
    parser.add_argument('-r', dest='min_radius', type=int,\
                        help="Minimal radius for considered pixels")
    parser.add_argument('--rsoft', dest='min_radius_soft', type=int,\
                        help="Minimal radius for orientationally relevant pixels")
    parser.add_argument('-R', dest='max_radius', type=int,\
                        help="Maximum radius for considered pixels")
    parser.add_argument('--wavelength', dest='wavelength', required=True, type=float,\
                        help="Radiation wavelength (Angstrom)")
    parser.add_argument('--distance', dest='distance', required=True, type=float,\
                        help="Detector distance (meters)")
    parser.add_argument('--pixel', dest='pixel', required=True, type=float,\
                        help="Pixel size (meters)")
    parser.add_argument('--polarisation', dest='polarisation', choices=['x', 'y', 'none'], \
                        default='x', help="Detector polarisation")
    parser.add_argument('--q_mul', dest='q_mul', type=float,\
                        help="Global q multiplier")
    parser.add_argument('--hdf', action='store_true', help="Use HDF5 Dragonfly format")
    parser_args = parser.parse_args()

    cxi_file = parser_args.cxi_file
    out_dir = parser_args.dir
    binning = parser_args.binning
    min_radius = parser_args.min_radius
    min_radius_soft = parser_args.min_radius_soft
    max_radius = parser_args.max_radius
    wavelength = parser_args.wavelength
    distance = parser_args.distance
    pixel = parser_args.pixel
    polarisation = parser_args.polarisation
    q_mul = parser_args.q_mul
    hdf = parser_args.hdf

    if not os.path.isfile(cxi_file):
        parser.error("File {} doesn't exist".format(cxi_file))

    if not os.path.isdir(out_dir):
        parser.error("Directory {} doesn't exist".format(out_dir))

    for i in [min_radius, min_radius_soft, max_radius]:
        if i is not None and i < 0:
            parser.error("Negative radius is not allowed")

    print('Add CXI data {} to directory: {}'.format(cxi_file, out_dir))
    if binning:
        print('Binning: {}'.format(binning))
    if min_radius:
        print('Min radius: {}'.format(min_radius))
    if min_radius_soft:
        print('Soft min radius: {}'.format(min_radius_soft))
    if max_radius:
        print('Max radius: {}'.format(max_radius))
    print('Wavelength: {} Angstrom'.format(wavelength))
    print('Detector distance: {} m'.format(distance))
    print('Pixel size: {} m'.format(pixel))
    print('Detector polarisation: {}'.format(polarisation))
    if q_mul:
        print('Global q multiplier: {}'.format(q_mul))
    print('Create HDF5 files: {}'.format('Yes' if hdf else 'No'))

    config = configparser.ConfigParser()
    config_fname = os.path.join(out_dir, 'config.ini')
    config.read(config_fname)

    cxi_fname = os.path.basename(cxi_file)

    input_data_dir = os.path.join(out_dir, config['emc']['output_folder'], 'input')
    os.makedirs(input_data_dir, exist_ok=True)

    out_file_base = os.path.join(input_data_dir, cxi_fname.split('.')[0])

    result = convert_data(cxi_file, out_file_base, wavelength, distance, pixel, polarisation,
                          binning, min_radius, min_radius_soft, max_radius, q_mul, hdf)

    emclist_file = os.path.join(out_dir, config['emc']['in_photons_list'])
    detlist_file = os.path.join(out_dir, config['emc']['in_detector_list'])

    with open(emclist_file, 'a+') as emclist:
        for emc_file in result['emc_files']:
            emclist.write(emc_file[len(out_dir)+1:] + '\n')

    with open(detlist_file, 'a+') as detlist:
        for det_file in result['det_files']:
            detlist.write(det_file[len(out_dir)+1:] + '\n')


if __name__ == '__main__':
    main()
