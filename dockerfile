FROM python:3

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY dist/dragonfly_wrapper*tar.gz .
RUN pip install dragonfly_wrapper*

WORKDIR /work