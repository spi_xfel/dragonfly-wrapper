# /bin/sh

docker login --username=sbobkov
docker tag dragonfly_wrapper spixfel/dragonfly_wrapper
docker push spixfel/dragonfly_wrapper
docker tag dragonfly_utils spixfel/dragonfly_utils
docker push spixfel/dragonfly_utils
docker tag dragonfly spixfel/dragonfly
docker push spixfel/dragonfly
docker logout

