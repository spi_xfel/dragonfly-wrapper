#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse

IMAGE_NAME = 'spixfel/dragonfly'


def emc_cmd(container, workdir, it, resume, detached):
    cmd = 'docker run -v {0}:/work -w /work '.format(workdir) + \
          ('-d ' if detached else '-i -t ') + \
          '{} /src/emc {} {:d}'.format(container, '-r' if resume else '', it)

    return cmd

def main():
    parser = argparse.ArgumentParser(description='Run Dragonfly EMC through docker container')
    parser.add_argument('work', help="Working directory")
    parser.add_argument('it', type=int, help="Number of iterations")
    parser.add_argument('-r', '--resume', action='store_true',
                        help="Resume previous reconstruction")
    parser.add_argument('-d', dest='detached', action='store_true', help="Run detached contained")
    parser_args = parser.parse_args()

    workdir = os.path.abspath(parser_args.work)
    it = parser_args.it
    resume = parser_args.resume
    detached = parser_args.detached

    cmd = emc_cmd(IMAGE_NAME, workdir, it, resume, detached)

    print(cmd)
    os.system(cmd)


if __name__ == '__main__':
    main()
