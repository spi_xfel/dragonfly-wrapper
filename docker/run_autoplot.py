#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse

IMAGE_NAME = 'spixfel/dragonfly_utils'

def main():
    parser = argparse.ArgumentParser(description='Run Dragonfly autoplot through docker container')
    parser.add_argument('work', help="Working directory")
    parser_args = parser.parse_args()

    workdir = os.path.abspath(parser_args.work)

    if not os.getenv('DISPLAY'):
        parser.error('DISPLAY is not set, X server is not available')
    elif 'localhost' in os.getenv('DISPLAY'):
        display_num = int(os.getenv('DISPLAY').split(':')[-1].split('.')[0])
        socat_cmd = \
            'socat TCP4:localhost:60{0} UNIX-LISTEN:/tmp/.X11-unix/X{0} &'.format(display_num)
        os.system(socat_cmd)
        os.environ["DISPLAY"] = ":{}".format(display_num)
        cmd = 'docker run -v {0}:/work -w /work -it '.format(workdir) + \
              '-e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix ' + \
              '-v $HOME/.Xauthority:/home/.Xauthority -e HOME=/home --hostname $(hostname) ' +\
              '{} python3 /src/autoplot.py'.format(IMAGE_NAME)
    else:
        cmd = 'docker run -v {0}:/work -w /work -it '.format(workdir) + \
              '-e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix ' + \
              '{} python3 /src/autoplot.py'.format(IMAGE_NAME)

    os.system(cmd)


if __name__ == '__main__':
    main()
