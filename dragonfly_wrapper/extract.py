#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Detection of best iteration in Dragonfly result and conversion into CXI format
Author: Sergey Bobkov
"""

import math
import numpy as np
import h5py

def parse_emc_log(log_file):
    """Extract reconstruction values from the log file

    Keyword arguments:
    log_file -- log file name

    Return:
    result -- dict with extracted parameters.
    Each parameter is a single value or a list of values for each iteration.
    """
    result = {}
    with open(log_file, 'r') as lfile:
        for line in lfile:
            if not line:
                continue
            elif line[0] == ' ':
                key, _, value = line.split()
                result[key] = value
            elif line[:4] == 'Iter':
                param_names = line.split()
                for name in param_names:
                    result[name] = []
            elif line[0].isdigit():
                param_values = line.split()
                for name, value in zip(param_names, param_values):
                    result[name].append(float(value))

    return result


def get_best_iteration(beta_values, rms_values, quality):
    """Get index of iteration with best convergance according to measurements

    Keyword arguments:
    beta_values -- list of beta values
    rms_values -- list of RMS values
    quality -- arbitrary quality measurement

    Return:
    index -- index of best iteration
    """
    if max(beta_values) != min(beta_values):
        considered_iterations = [i
                                 for i in range(len(beta_values) - 1)
                                 if beta_values[i] != beta_values[i+1]]
        considered_iterations.append(len(beta_values) - 1)
    else:
        considered_iterations = range(len(beta_values))

    for i in considered_iterations:
        if rms_values[i] > rms_values[0] or math.isnan(rms_values[i]):
            considered_iterations.remove(i)

    max_qual = max(qual
                   for i, qual in enumerate(quality)
                   if i in considered_iterations)
    return quality.index(max_qual)

def read_h5(input_emc_h5):
    """Read EMC result in hdf5 format

    Keyword arguments:
    input_emc_h5 -- EMC result file

    Return:
    intens -- 3D intensity data
    """
    with h5py.File(input_emc_h5, 'r') as h5file:
        intens = h5file['intens'][0]

    return intens


def read_bin(input_emc_bin):
    """Read EMC result in binary format

    Keyword arguments:
    input_emc_bin -- EMC result file

    Return:
    intens -- 3D intensity data
    """

    intens = np.fromfile(input_emc_bin)

    intens_len = intens.shape[0]

    size = round(math.pow(intens_len, 1/3))
    if size**3 != intens_len:
        raise ValueError('Cannot estimate 3D size for binary file with len {}'.format(intens_len))

    return intens.reshape((size,size,size))


def save_cxi(intens, output_cxi):
    """Save 3D intensity into CXI format

    Keyword arguments:
    intens -- intensity data
    output_cxi -- name of output CXI file
    """
    with h5py.File(output_cxi, 'w') as h5file:
        h5file['cxi_version'] = 160
        entry = h5file.create_group('entry_1')
        image = entry.create_group('image_1')
        image.create_dataset('data', data=intens)
        image.create_dataset('data_type', data="unphased amplitude")
        image.create_dataset('data_space', data="diffraction")

        entry['data_1'] = h5py.SoftLink('/entry_1/image_1')
