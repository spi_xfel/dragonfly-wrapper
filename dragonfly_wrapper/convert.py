#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Convertion from CXI to Dragonfly format
Author: Sergey Bobkov
"""

import os
import numpy as np
import h5py


def apply_binning(data, binning, mask=None):
    """Apply binning to the data. Combine groups of BIN*BIN pixels together.
    Value of binned pixel in data is the sum of all pixels in binned group.
    Value of binned pixel in mask - maximum value in binned group.

    Keyword arguments:
    data -- 2D image
    binning -- binning value, size of group side.
    mask -- 2D mask for same image.

    Return:
    binned_data -- binning result for data
    binned_mask -- if mask is provided, function return tuple (binned_data, binned_mask)
    """
    size_y, size_x = data.shape

    bin_x = size_x // binning #+ (1 if X%bin_value > bin_value//2 else 0)
    bin_y = size_y // binning #+ (1 if Y%bin_value > bin_value//2 else 0)

    binned_data = data[:bin_y*binning, :bin_x*binning]
    binned_data = binned_data.reshape(bin_y, binning, bin_x, binning).sum(axis=(1, 3))

    if mask is None:
        return binned_data
    else:
        binned_mask = mask[:bin_y*binning, :bin_x*binning]
        binned_mask = np.amax(binned_mask.reshape(bin_y, binning, bin_x, binning), axis=(1, 3))
        return binned_data, binned_mask


def _compute_polarization(polarization, px, py, norm):
    if polarization.lower() == 'x':
        return 1. - (px*px)/(norm*norm)
    elif polarization.lower() == 'y':
        return 1. - (py*py)/(norm*norm)
    elif polarization.lower() == 'none':
        return 1. - (px*px + py*py)/(2*norm*norm)
    else:
        assert False


def _create_detector_file(basename, pixel_mask, image_center, wavelength,\
                          distance, pixel_size, polarisation, q_mul, hdf=True):
    # 0 - good
    # 1 - non-orientationally relevant pixels
    # 2 - bad

    size_y, size_x = pixel_mask.shape
    center_x, center_y = image_center[:2]

    y_vals = np.arange(size_y, dtype=float) - center_y
    x_vals = np.arange(size_x, dtype=float) - center_x

    x_grid, y_grid = np.meshgrid(x_vals, y_vals)

    x_grid = x_grid.flatten()
    y_grid = y_grid.flatten()
    pixel_mask = pixel_mask.flatten()

    ewald_r = distance / pixel_size
    q_scaling = 2 * np.pi / wavelength

    if q_mul is None:
        angle_step = np.arctan(pixel_size / distance)
        q_step = 2. * np.sin(0.5 * angle_step) * 2. * np.pi / wavelength
        q_mul = 1. / q_step

    q_scaling *= q_mul

    px_grid = pixel_size*x_grid
    py_grid = pixel_size*y_grid

    norm_grid = np.sqrt(px_grid**2 + py_grid**2 + distance**2)

    pixel_polar = _compute_polarization(polarisation, px_grid, py_grid, norm_grid)

    qx_grid = px_grid*q_scaling/norm_grid
    qy_grid = py_grid*q_scaling/norm_grid
    qz_grid = q_scaling*(distance/norm_grid - 1.)*(distance/norm_grid)

    solid_angle = distance*(pixel_size**2) / np.power(norm_grid, 3)
    solid_angle *= pixel_polar

    # Skip bad pixels
    apply_filter = pixel_mask != 2
    qx_grid = qx_grid[apply_filter]
    qy_grid = qy_grid[apply_filter]
    qz_grid = qz_grid[apply_filter]
    solid_angle = solid_angle[apply_filter]
    pixel_mask = pixel_mask[apply_filter]

    qmax = np.sqrt(qx_grid**2 + qy_grid**2 + qz_grid**2).max()

    if hdf:
        with h5py.File(basename, 'w') as h5file:
            h5file.create_dataset('qx', data=qx_grid)
            h5file.create_dataset('qy', data=qy_grid)
            h5file.create_dataset('qz', data=qz_grid)
            h5file.create_dataset('corr', data=solid_angle)
            h5file.create_dataset('mask', data=pixel_mask)
            h5file.create_dataset('detd', data=ewald_r)
            h5file.create_dataset('ewald_rad', data=q_scaling)
    else:
        with open(basename, "w") as fp:
            fp.write('{:d} {:f} {:f}\n'.format(len(qx_grid), ewald_r, q_scaling))
            for vals in zip(qx_grid, qy_grid, qz_grid, solid_angle, pixel_mask):
                line = "{:21.15e} {:21.15e} {:21.15e} {:21.15e} {:d}\n".format(*vals)
                fp.write(line)

    return qmax


def _mask_pixels_inside_circle(shape, center, radius):
    size_y, size_x = shape
    center_x, center_y = center[:2]
    y_vals = np.arange(size_y, dtype=float)
    x_vals = np.arange(size_x, dtype=float)

    x_grid, y_grid = np.meshgrid(x_vals, y_vals)
    x_grid -= center_x
    y_grid -= center_y
    r_grid = np.sqrt(x_grid**2 + y_grid**2)

    return r_grid < radius


class EMC_writer():
    """
    Class for saving data in old Dragonfly format. Copied from Dragonfly source.
    """
    def __init__(self, out_fname, num_pix):
        out_folder = os.path.dirname(out_fname)
        temp_fnames = [os.path.join(out_folder, fname) 
                       for fname in ['temp.po', 'temp.pm', 'temp.cm']]
        self.f = [open(fname, 'wb') for fname in temp_fnames]

        self.out_fname = out_fname
        self.num_data = 0
        self.num_pix = num_pix
        self.mean_count = 0.
        self.ones = []
        self.multi = []

    def write_frame(self, frame):
        place_ones = np.where(frame == 1)[0]
        place_multi = np.where(frame > 1)[0]
        count_multi = frame[place_multi]

        self.num_data += 1
        self.mean_count += len(place_ones) + count_multi.sum()
        self.ones.append(len(place_ones))
        self.multi.append(len(place_multi))

        place_ones.astype(np.int32).tofile(self.f[0])
        place_multi.astype(np.int32).tofile(self.f[1])
        count_multi.astype(np.int32).tofile(self.f[2])

    def finish_write(self):
        for fp in self.f:
            fp.close()

        if self.num_data == 0:
            for fp in self.f:
                os.system('rm ' + fp.name)
            return

        self.mean_count /= self.num_data
        self.ones = np.array(self.ones)
        self.multi = np.array(self.multi)

        fp = open(self.out_fname, 'wb')
        header = np.zeros((256), dtype='i4')
        header[0] = self.num_data
        header[1] = self.num_pix
        header.tofile(fp)
        self.ones.astype('i4').tofile(fp)
        self.multi.astype('i4').tofile(fp)
        fp.close()
        for fp in self.f:
            os.system('cat ' + fp.name + ' >> ' + self.out_fname)
            os.system('rm ' + fp.name)


def _create_emc_file(basename, dataset):
    n_frames, size = dataset.shape

    emcwriter = EMC_writer(basename, size)

    for i in range(n_frames):
        photons = dataset[i]
        photons[photons < 0] = 0
        emcwriter.write_frame(photons.flatten())

    emcwriter.finish_write()


def _create_emc_file_hdf(basename, dataset):
    n_frames, size = dataset.shape

    with h5py.File(basename, 'w') as h5file:
        h5file.create_dataset('num_pix', data=size)
        # verlen_int = h5py.vlen_dtype(np.dtype('int32'))
        verlen_int = h5py.special_dtype(vlen='int32')
        count_multi = h5file.create_dataset('count_multi', (n_frames,), dtype=verlen_int)
        place_multi = h5file.create_dataset('place_multi', (n_frames,), dtype=verlen_int)
        place_ones = h5file.create_dataset('place_ones', (n_frames,), dtype=verlen_int)

        for i, photons in enumerate(dataset):
            photons[photons < 0] = 0
            place_ones[i] = np.where(photons == 1)[0]
            place_multi[i] = np.where(photons > 1)[0]
            count_multi[i] = photons[place_multi[i]]


def convert_data(cxi_file, out_file_base, wavelength, distance, pixel_size, polarisation,
                 binning=None, min_radius=None, min_radius_soft=None, max_radius=None,
                 q_mul=None, hdf=True):
    """Convert cxi data in EMC format

    Keyword arguments:
    cxi_file -- input CXI file
    out_file_base -- start part of output file name, will be appended with ending with extension
    wavelength -- radiation wavelenght (Angstrom)
    distance -- distance between detector and interaction point (m)
    pixel_size -- size of one detector pixel (m)
    polarisation -- detector polarisation ['x', 'y' or 'none']
    binning -- binning value, size of group side.
    min_radius -- inner radius of image area to convert.
    min_radius_soft -- pixels within min_radius_soft will not be used for orientation determination
    max_radius -- outer radius of image area. Pixels outside max_radius will be ignored.
    q_mul -- global multiplier for q-space, disables autoscaling
    for proper combination of several input files.
    hdf -- save output in new HDF5 Dragonfly format

    Return:
    dict -- {'qmax', 'det_files', 'emc_files'} 'qmax' - list of maximum q_values in converted data.
    'det_files' & 'emc_files' - names of result files.
    """
    qmax_list = []
    det_files = []
    emc_files = []

    with h5py.File(cxi_file, 'r') as h5file:
        entry = h5file['entry_1']
        imagesets = [k for k in entry.keys() if k.startswith('image_')]
        for i, k in enumerate(imagesets):
            if len(imagesets) > 1:
                det_file = '{}_{}_det'.format(out_file_base, i+1)
                emc_file = '{}_{}_photons'.format(out_file_base, i+1)
            else:
                det_file = out_file_base + '_det'
                emc_file = out_file_base + '_photons'

            if hdf:
                det_file += '.h5'
                emc_file += '.h5'
            else:
                det_file += '.dat'
                emc_file += '.emc'

            det_files.append(det_file)
            emc_files.append(emc_file)

            image = entry[k]
            data = image['data']
            mask = image['mask'][:].astype(int)
            center = image['image_center'][:]

            assert len(data.shape) == 3
            assert len(mask.shape) == 2
            assert data.shape[1:] == mask.shape
            assert center.shape == (3,)

            n_frames = len(data)

            mask[mask > 0] = 2
            # Mask pixels within min_radius_soft as non relevant for orientations
            if min_radius_soft is not None:
                mask[(mask == 0) * _mask_pixels_inside_circle(
                    mask.shape, center, min_radius_soft)] = 1

            # Mask pixels inside q_min as bad ones
            if min_radius is not None:
                mask[_mask_pixels_inside_circle(mask.shape, center, min_radius)] = 2

            # Mask pixels outside q_max as bad ones
            if max_radius is not None:
                mask[False == _mask_pixels_inside_circle(
                    mask.shape, center, max_radius)] = 2

            if binning is None:
                binning = 1

            binned_data, binned_mask = apply_binning(data[0], binning, mask)
            binned_center = tuple(x/binning for x in center)

            qmax = _create_detector_file(det_file, binned_mask,
                                         image_center=binned_center,
                                         wavelength=wavelength,
                                         distance=distance,
                                         pixel_size=pixel_size*binning,
                                         polarisation=polarisation,
                                         q_mul=q_mul,
                                         hdf=hdf)

            qmax_list.append(qmax)

            emc_frame = binned_data[binned_mask != 2]
            emc_data = np.zeros((n_frames, len(emc_frame)), dtype=data.dtype)
            emc_data[0] = emc_frame

            for j in range(1, n_frames):
                emc_data[j] = apply_binning(data[j], binning)[binned_mask != 2]

            if hdf:
                _create_emc_file_hdf(emc_file, emc_data)
            else:
                _create_emc_file(emc_file, emc_data)

    return {'qmax': qmax_list, 'det_files': det_files, 'emc_files': emc_files}
